/*global google*/
import React, { Component } from "react";
import {
  withGoogleMap,
  withScriptjs,
  GoogleMap,
  DirectionsRenderer
} from "react-google-maps";
import Geocode from "react-geocode";

class MapRoute extends Component {
  state = {
    directions: null,
    origin: null,
    destination: null,
  };

  componentDidMount() {
    Geocode.setApiKey("AIzaSyDms0AAI98NfOeZTqcTVgStPtAytmf3XGI");
    Geocode.setLanguage("en");

    const directionsService = new google.maps.DirectionsService();
    const origin = { lat: 40.756795, lng: -73.954298 };
    const destination = { lat: 41.756795, lng: -78.954298 };

    // const origin = this.getLocation(this.props.origin);
    // const destination = this.getLocation(this.props.destination);

    directionsService.route(
      {
        origin: origin,
        destination: destination,
        travelMode: google.maps.TravelMode.DRIVING
      },
      (result, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.setState({
            directions: result
          });
        } else {
          console.error(`error fetching directions ${result}`);
        }
      }
    );
  }

  getLocation(address){
    Geocode.fromAddress(address).then(
        response => {
          const { lat, lng } = response.results[0].geometry.location;
          console.log(">>>", lat,lng);
          return {lat, lng};
        },
        error => {
          console.error(error);
        }
      );
  }

  render() {
    const GoogleMapExample = withGoogleMap(props => (
      <GoogleMap
        defaultCenter={{ lat: 30.566991, lng: 7.857358 }}
        defaultZoom={2}
      >
        <DirectionsRenderer
          directions={this.state.directions}
        />
      </GoogleMap>
    ));

    return (
      <div>
        <GoogleMapExample
          containerElement={<div style={{ height: `700px`, width: "800px" }} />}
          mapElement={<div style={{ height: `100%` }} />}
        />
      </div>
    );
  }
}

export default MapRoute;
